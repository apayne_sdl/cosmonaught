# Copyright Statement

As an employee of Utah State University Research Foundation – Space Dynamics
Laboratory, Alexander Payne has signed an agreement to assign to USURF – SDL any
intellectual property developed in association with his work for the foundation.

All copyright is therefore assigned to Utah State University Research Foundation
– Space Dynamics Laboratory.
