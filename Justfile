################################################################################
#                                   Justfile                                   #
#                                                                              #
# This file manages actions routinely taken during development. It is used by  #
# the utility `just`. This utility, at time of writing, only uses POSIX-like   #
# shells, and does not use Windows PowerShell or CMD.                          #
#                                                                              #
#                               Revision History                               #
# 2019-11-06   A. Payne        / SDL   Creation                                #
################################################################################
