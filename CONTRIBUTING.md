# Contributing to Cosmonaught

The primary author, Alexander Payne, writes Cosmonaught as a personal project on
personal time, but motivated and influenced by material and tasks encountered at
his work as an employee of Space Dynamics Laboratory. Cosmonaught’s source is
available as required by the upstream projects on which it is based, as well as
the fact its primary author is an employee of a public university. However,
because it is also interlinked with work for the United States government,
direct external contributions (patches, pull requests, or other material
delivery) are not necessarily able to be accepted.

If in the future external contributions are able to be accepted, they still must
transfer copyright to Utah State University Research Foundation – Space Dynamics
Laboratory.
