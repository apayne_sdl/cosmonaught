<div align="center">

# Cosmonaught Project

</div>

Cosmonaught is a utility suite that serves the development and operation of
asymmetric, heterogenous, device networks. It is primarily intended for use in
satellite operations, and has specialized components for working with `xTEDS`
XML manifests, the [COSMOS] framework, and the Space Plug-and-Play Architecture
application framework.

This project is primarily written by Alexander Payne, an employee of Utah State
University Research Foundation – Space Dynamics Laboratory, on personal time.

## Revision History

|    Date    |  Author  | Summary                                              |
|:-----------|:---------|:-----------------------------------------------------|
| 2019-11-06 | A. Payne | Creation.                                            |

[COSMOS]: //cosmosrb.com
