# Cosmonaught Code of Conduct

Cosmonaught is subject to the following codes of behavior, conduct, and ethics.
In the event that these documents conflict, they are listed in descending
priority.

1. [Space Dynamics Laboratory Code of Business Ethics][sdl-cbe]
1. [IEEE Code of Ethics][ieee-coe]
1. [Rust Code of Conduct][rust-coc]

[ieee-coe]: https://www.ieee.org/about/corporate/governance/p7-8.html
[rust-coc]: https://www.rust-lang.org/conduct.html
[sdl-cbe]: ./assets/usurf-hr-008m.pdf
